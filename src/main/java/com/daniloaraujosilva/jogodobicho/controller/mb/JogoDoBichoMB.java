package com.daniloaraujosilva.jogodobicho.controller.mb;

import java.io.Serializable;
import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.log4j.Logger;

import com.daniloaraujosilva.jogodobicho.model.bo.JogoDoBichoBO;
import com.daniloaraujosilva.jogodobicho.model.helper.FacesMessager;
import com.daniloaraujosilva.jogodobicho.model.pojo.NodePojo;

/**
 * 
 */
@ViewScoped
@ManagedBean(name="jogoDoBichoMB")
public class JogoDoBichoMB implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3298340293849150158L;
	
	/**
	 * 
	 */
	private Logger logger = Logger.getLogger(getClass());
	
	/**
	 * Objeto de negócios. Utilizado normalmente com DAOs e entidades.
	 * As regras de negócio do jogo são delegadas a essa classe.
	 */
	private JogoDoBichoBO bo;
	
	/**
	 * Permissões, neste caso da tela.
	 * Indica o que será ou não renderizado.
	 */
	private HashMap<String, Boolean> permissions;

	/**
	 * 
	 */
	public JogoDoBichoMB() {
		super();
	}
	
	/**
	 * 
	 */
	@PostConstruct
	public void init() {
		try {
			permissions = new HashMap<String, Boolean>();
			permissions.put("escolhendoAnimal", true);
			
			bo = new JogoDoBichoBO();
			bo.start();
		} catch (Exception exception) {
			manageThrowable(exception);
		}
	}

	/**
	 * 
	 * @return
	 */
	public NodePojo getCurrent() {
		return bo.getCurrent();
	}

	/**
	 * 
	 * @return
	 */
	public HashMap<String, Boolean> getPermissions() {
		return permissions;
	}

	/**
	 * 
	 * @param permissions
	 */
	public void setPermissions(HashMap<String, Boolean> permissions) {
		this.permissions = permissions;
	}

	/**
	 * 
	 * @return
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	/**
	 * 
	 */
	public void salvarERecomecar() {
		try {
			permissions.clear();
			permissions.put("escolhendoAnimal", true);
			bo.save();
			bo.start();
		} catch (Exception exception) {
			manageThrowable(exception);
		}
	}
	
	/**
	 * 
	 */
	public void animalEscolhido() {
		try {
			permissions.clear();
			permissions.put("perguntandoSeAnimalEscolhidoTemHabilidade", true);
		} catch (Exception exception) {
			manageThrowable(exception);
		}
	}
	
	/**
	 * 
	 */
	public void animalPossuiAHabilidade() {
		try {
			permissions.clear();
			bo.moveToYesNode();
			if (bo.getCurrent().isLeaf()) {
				permissions.put("perguntandoSeEOAnimalEscolhido", true);
			} else {
				permissions.put("perguntandoSeAnimalEscolhidoTemHabilidade", true);
			}
		} catch (Exception exception) {
			manageThrowable(exception);
		}
	}
	
	/**
	 * 
	 */
	public void animalNaoPossuiAHabilidade() {
		try {
			permissions.clear();
			if (bo.getCurrent().isLeaf()) {
				permissions.put("perguntandoQualEOAnimal", true);
				bo.createAndMoveToNewNode();
			} else {
				bo.moveToNoNode();
				if (bo.getCurrent().isLeaf()) {
					permissions.put("perguntandoSeEOAnimalEscolhido", true);
				} else {
					permissions.put("perguntandoSeAnimalEscolhidoTemHabilidade", true);
				}
			}
		} catch (Exception exception) {
			manageThrowable(exception);
		}
	}
	
	/**
	 * 
	 */
	public void animalEscolhidoEncontrado() {
		try {
			permissions.clear();
			permissions.put("animalEscolhidoEncontrado", true);
		} catch (Exception exception) {
			manageThrowable(exception);
		}
	}
	
	/**
	 * 
	 */
	public void animalEscolhidoNaoEncontrado() {
		try {
			permissions.clear();
			if (bo.getCurrent().isLeaf()) {
				bo.createAndMoveToNewNode();
				permissions.put("perguntandoQualEOAnimal", true);
			} else {
				bo.moveToYesNode();
				permissions.put("perguntandoSeAnimalEscolhidoTemHabilidade", true);
			}
		} catch (Exception exception) {
			manageThrowable(exception);
		}
	}
	
	/**
	 * 
	 */
	public void adicionandoNovoAnimal() {
		try {
			permissions.clear();
			permissions.put("perguntandoAHabilidadeDoNovoAnimal", true);
		} catch (Exception exception) {
			manageThrowable(exception);
		}
	}
	
	/**
	 * Limpa o banco de dados da aplicação.
	 */
	public void apagarInformacoes() {
		try {
			bo.clear();
			permissions.clear();
			permissions.put("escolhendoAnimal", true);
			FacesMessager.addInfo("As informações previamente cadastradas foram removidas com sucesso.");
		} catch (Exception exception) {
			manageThrowable(exception);
		}
	}
	
	/**
	 * Retorna os dados guardados em formato json da aplicação para o estado original.
	 */
	public void retornarAoOriginal() {
		try {
			bo.backToDefaults();
			permissions.clear();
			permissions.put("escolhendoAnimal", true);
			FacesMessager.addInfo("As informações foram retornadas ao seu estado original.");
		} catch (Exception exception) {
			manageThrowable(exception);
		}
	}
	
	/**
	 * Este método tem o intuito de gerenciar todas as exceções do bean.
	 * Como esta foi uma implementação simples não cheguei ir além.
	 * Mas o intuito seria chamar um gerenciador global de exceções que
	 * 	captura e trata todas as exceções ou throwables do sistema, mostrando
	 * 	sempre uma mensagem tratada para o usuário.
	 * 
	 * @param throwable
	 */
	private void manageThrowable(Throwable throwable) {
		logger.error(throwable);
	}
}