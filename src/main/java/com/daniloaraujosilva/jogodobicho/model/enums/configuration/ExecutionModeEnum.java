/**
 *
 */
package com.daniloaraujosilva.jogodobicho.model.enums.configuration;

/**
 *
 */
public enum ExecutionModeEnum {

	/**
	 *
	 */
	CONSOLE("console", "Java Console", "Java console execution."),

	/**
	 *
	 */
	SERVER("server", "Application Server", "Application server online execution."),

	/**
	 *
	 */
	TEST("test", "Unit Test", "Unit test execution");

	/**
	 *
	 */
	private String initials;

	/**
	 *
	 */
	private String title;

	/**
	 *
	 */
	private String description;


	/**
	 *
	 * @param initials
	 * @param title
	 * @param description
	 */
	private ExecutionModeEnum(String initials, String title, String description) {
		this.initials = initials;
		this.title = title;
		this.description = description;
	}

	/**
	 *
	 * @return
	 */
	public String getInitials() {
		return initials;
	}

	/**
	 *
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 *
	 * @return
	 */
	public String getDescription() {
		return description;
	}
}