/**
 *
 */
package com.daniloaraujosilva.jogodobicho.model.configuration;

import javax.servlet.ServletContext;

import com.daniloaraujosilva.jogodobicho.model.enums.configuration.ExecutionModeEnum;

/**
 *
 */
public class General {

	/**
	 *
	 */
	private static ExecutionModeEnum executionMode;

	/**
	 *
	 */
	private static ServletContext servletContext;

	/**
	 *
	 * @return
	 */
	public static ExecutionModeEnum getExecutionMode() {
		return executionMode;
	}

	/**
	 *
	 * @param executionMode
	 */
	public static void setExecutionMode(ExecutionModeEnum executionMode) {
		if (General.executionMode == null) {
			General.executionMode = executionMode;
		}
	}

	/**
	 *
	 * @return
	 */
	public static ServletContext getServletContext() {
		return servletContext;
	}

	/**
	 *
	 * @param servletContext
	 */
	public static void setServletContext(ServletContext servletContext) {
		if (General.servletContext == null) {
			General.servletContext = servletContext;
		}
	}
}