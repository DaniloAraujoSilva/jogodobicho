package com.daniloaraujosilva.jogodobicho.model.override.javax.servlet;

import javax.servlet.ServletContextEvent;
import javax.servlet.annotation.WebListener;

import com.daniloaraujosilva.jogodobicho.model.configuration.General;
import com.daniloaraujosilva.jogodobicho.model.enums.configuration.ExecutionModeEnum;

/**
 *
 */
@WebListener
public class ServletContextListener implements javax.servlet.ServletContextListener {

	/**
	 *
	 */
	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		try {
			General.setExecutionMode(ExecutionModeEnum.SERVER);
			General.setServletContext(servletContextEvent.getServletContext());
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	/**
	 *
	 */
	@Override
	public void contextDestroyed(ServletContextEvent servletContextEvent) {
	}
}