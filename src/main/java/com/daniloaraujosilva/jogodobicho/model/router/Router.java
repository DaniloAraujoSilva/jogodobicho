package com.daniloaraujosilva.jogodobicho.model.router;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;

import org.omnifaces.util.Faces;

import com.daniloaraujosilva.jogodobicho.model.configuration.General;
import com.daniloaraujosilva.jogodobicho.model.enums.configuration.ExecutionModeEnum;

/**
 * Classe para encontrar rotas no sistema.
 */
public class Router {

	/**
	 *
	 */
	private HashMap<String, Object> map;

	/**
	 *
	 */
    private Router() {
    	try {
    		initialize();
    	} catch (Exception exception) {
    		exception.printStackTrace();
    	}
    }

    /**
     *
     * @return
     */
    public static Router getInstance() {
        return Singleton.INSTANCE;
    }

    /**
     *
     * @return
     */
    public HashMap<String, Object> get() {
    	return map;
    }

    /**
     *
     * @param key
     * @return
     */
    public File get(String key) {
    	return (File) map.get(key);
    }

    /**
     *
     * @param basePath
     * @param relativePath
     * @return
     */
    public File get(String basePath, String relativePath) {
    	return new File(get(basePath), relativePath);
    }

    /**
     *
     */
    private static class Singleton {

        private static final Router INSTANCE = new Router();
    }

    /**
	 * Checks if is deployed.
	 *
	 * @return the boolean
	 */
    public Boolean isDeployed() {
    	if (!map.containsKey("isDeployed")) {
    		if (ExecutionModeEnum.SERVER.equals(General.getExecutionMode())) {
        		map.put("isDeployed", true);
    		} else {
    			map.put("isDeployed", false);
    		}
    	}

    	return (Boolean) map.get("isDeployed");
    }

    /**
     * Inicializa as rotas do sistema.
     * É necessário um tratamento diferente, dependendo do servidor.
     *
     * Obs.: a aplicação poderia funcionar em Java Console ou jUnit com algumas modificações,
     * 	no entanto para esta implementação simples isso não foi contemplado.
     *
     * @throws UnsupportedEncodingException
     * @throws MalformedURLException
     */
    private void initialize() throws UnsupportedEncodingException, MalformedURLException {
    	map = new HashMap<String, Object>();

		File root = null;
    	if (isDeployed()) {
    		URL rootUrl = Faces.getServletContext().getResource("/");
        	File rootPath;
        	if (System.getProperty("catalina.home") != null) {
        		URL routerUrl = Router.class.getResource("/");
        		String temp = URLDecoder.decode(routerUrl.getFile(), "UTF-8");
        		rootPath = new File(temp).getParentFile().getParentFile();
    		} else if (System.getProperty("jboss.home.dir") != null) {
    			rootPath = new File(rootUrl.getFile());
    		} else {
    			throw new RuntimeException("Application server not recognized.");
    		}

    		map.put("/", rootPath);
    		map.put("/src", null); // Não existe neste contexto.
    		map.put("/src/main/java", new File(get("/"), "WEB-INF/classes"));
    		map.put("/src/main/resources", new File(get("/"), "WEB-INF/classes"));
    		map.put("/src/main/resources/configuration", new File(get("/src/main/resources"), "configuration"));
    		map.put("/src/main/resources/configuration/profiles", new File(get("/src/main/resources/configuration"), "profiles"));
    		map.put("/src/main/webapp/web-inf", new File(get("/"), "WEB-INF"));
    		map.put("/src/main/webapp/meta-inf", new File(get("root"), "META-INF"));
    		map.put("/src/test/java", new File(get("/"), "WEB-INF/classes"));
    		map.put("/src/test/resources", new File(get("/"), "WEB-INF/classes"));
    	} else {
    		URL currentUrl = Router.class.getResource("/");
    		String temp = URLDecoder.decode(currentUrl.getFile(), "UTF-8");
    		root = new File(temp).getParentFile().getParentFile();

    		map.put("/", root);
    		map.put("/src", new File(get("/"), "src"));
    		map.put("/src/main/java", new File(get("/"), "/src/main/java"));
			map.put("/src/main/resources", new File(get("/"), "/src/main/resources"));
			map.put("/src/main/resources/configuration", new File(get("/src/main/resources"), "configuration"));
			map.put("/src/main/resources/configuration/profiles", new File(get("/src/main/resources/configuration"), "profiles"));
			map.put("/src/main/webapp/web-inf", new File(get("/"), "/src/main/webapp/web-inf"));
			map.put("/src/main/webapp/meta-inf", new File(get("root"), "/target/m2e-wtp/web-resources/META-INF"));
			map.put("/src/test/java", new File(get("/"), "/src/test/java"));
			map.put("/src/test/resources", new File(get("/"), "/src/test/resources"));
    	}
    }
}