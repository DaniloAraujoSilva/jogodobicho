package com.daniloaraujosilva.jogodobicho.model.bo;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import com.daniloaraujosilva.jogodobicho.model.helper.Serializer;
import com.daniloaraujosilva.jogodobicho.model.pojo.NodePojo;
import com.daniloaraujosilva.jogodobicho.model.router.Router;

/**
 *
 */
public class JogoDoBichoBO {

	/**
	 * Referencia o arquivo que contém o "banco de dados" da aplicação.
	 */
	private File file;

	/**
	 * Nó principal da árvore de dados.
	 */
	private NodePojo root;

	/**
	 * Nó corrente, refere-se ao animal que o usuário está tratando no momento.
	 */
	private NodePojo current;

	/**
	 *
	 */
	public JogoDoBichoBO() {
		super();
	}

	/**
	 *
	 * @return
	 */
	public NodePojo getRoot() {
		return root;
	}

	/**
	 *
	 * @param root
	 */
	public void setRoot(NodePojo root) {
		this.root = root;
	}

	/**
	 *
	 * @return
	 */
	public NodePojo getCurrent() {
		return current;
	}

	/**
	 *
	 * @param current
	 */
	public void setCurrent(NodePojo current) {
		this.current = current;
	}

	/**
	 *
	 * @throws IOException
	 */
	public void start() throws IOException {
		load();
	}

	/**
	 *
	 */
	public void moveToYesNode() {
		if (current.getYes() != null) {
			NodePojo last = current;
			current = current.getYes();
			current.setParent(last);
		}
	}

	/**
	 *
	 */
	public void moveToNoNode() {
		if (current.getNo() != null) {
			NodePojo last = current;
			current = current.getNo();
			current.setParent(last);
		}
	}

	/**
	 *
	 */
	public void createAndMoveToNewNode() {
		NodePojo newNode = new NodePojo();
		newNode.setYes(new NodePojo());
		newNode.getYes().setParent(newNode);
		newNode.setNo(current);
		newNode.setParent(current.getParent());
		if (current.getParent() != null) {
			if (current.isOnYesPath()) {
				current.getParent().setYes(newNode);
			} else {
				current.getParent().setNo(newNode);
			}
		}
		current = newNode;
	}

	/**
	 * Salva os dados da aplicação. Ou seja, salva novos cadastros que o usuário possa ter realizado.
	 *
	 * @throws IOException
	 */
	public void save() throws IOException {
		if (root != null) {
			String json = Serializer.getInstance().serialize(root);

			FileUtils.writeStringToFile(file, json, StandardCharsets.UTF_8);
		}
	}

	/**
	 * Limpa os dados da aplicação.
	 *
	 * @throws IOException
	 */
	public void clear() throws IOException {
		FileUtils.writeStringToFile(file, "", StandardCharsets.UTF_8);
		load();
	}

	/**
	 * Retorna os dados da aplicação para um padrão previamente definido, já com alguns animais.
	 *
	 * @throws IOException
	 */
	public void backToDefaults() throws IOException {
		File defaultDatabase = Router.getInstance().get("/src/main/resources", "database/default.json");
		String json = FileUtils.readFileToString(defaultDatabase, StandardCharsets.UTF_8);
		FileUtils.writeStringToFile(file, json, StandardCharsets.UTF_8);
		load();
	}

	/**
	 * Carrega os dados da aplicação.
	 *
	 * @throws IOException
	 */
	private void load() throws IOException {
		file = Router.getInstance().get("/src/main/resources", "database/current.json");

		if (!file.exists()) {
			file.createNewFile();
		}

		String json = FileUtils.readFileToString(file, StandardCharsets.UTF_8);

		if (StringUtils.isEmpty(json)) {
			root = new NodePojo();
			root.setQuestion("vive na água");
			root.setYes(new NodePojo());
			root.getYes().setQuestion("Tubarão");
			root.setNo(new NodePojo());
			root.getNo().setQuestion("Macaco");
		} else {
			root = Serializer.getInstance().unserialize(json, NodePojo.class);
		}

		current = root;
	}
}