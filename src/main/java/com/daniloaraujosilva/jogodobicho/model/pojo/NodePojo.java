package com.daniloaraujosilva.jogodobicho.model.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 
 */
public class NodePojo {
	
	/**
	 * 
	 */
	@JsonIgnore
	private NodePojo parent;
	
	/**
	 * Guarda uma questão que é feita ao usuário, seja de uma habilidade
	 * 	ou se é algum animal.
	 */
	private String question;

	/**
	 * 
	 */
	private NodePojo yes;
	
	/**
	 * 
	 */
	private NodePojo no;
	
	/**
	 * 
	 */
	public NodePojo() {
		super();
	}

	/**
	 * 
	 * @return
	 */
	public NodePojo getParent() {
		return parent;
	}

	/**
	 * 
	 * @param parent
	 */
	public void setParent(NodePojo parent) {
		this.parent = parent;
	}

	/**
	 * 
	 * @return
	 */
	public String getQuestion() {
		return question;
	}

	/**
	 * 
	 * @param question
	 */
	public void setQuestion(String question) {
		this.question = question;
	}

	/**
	 * 
	 * @return
	 */
	public NodePojo getYes() {
		return yes;
	}

	/**
	 * 
	 * @param yes
	 */
	public void setYes(NodePojo yes) {
		this.yes = yes;
	}

	/**
	 * 
	 * @return
	 */
	public NodePojo getNo() {
		return no;
	}

	/**
	 * 
	 * @param no
	 */
	public void setNo(NodePojo no) {
		this.no = no;
	}
	
	/**
	 * 
	 * @return
	 */
	public Boolean isRoot() {
		if (parent == null) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public Boolean isMiddle() {
		if (
			parent != null
			|| yes != null
			|| no != null
		) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public Boolean isLeaf() {
		if (
			yes == null
			&& no == null
		) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public Boolean isOnYesPath() {
		if (
			getParent() != null
			&& getParent().getYes().equals(this)
		) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public Boolean isOnNoPath() {
		if (
			getParent() != null
			&& getParent().getNo().equals(this)
		) {
			return true;
		} else {
			return false;
		}
	}
}