package com.daniloaraujosilva.jogodobicho.model.helper;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 * 
 */
public class FacesMessager {

	/**
	 * 
	 * @param detalhes
	 */
	public static void addInfo(String detalhes) {
		addMessage(FacesMessage.SEVERITY_INFO, "Informação", detalhes);
	}

	/**
	 * 
	 * @param detalhes
	 * @param identificador
	 */
	public static void addInfo(String detalhes, String identificador) {
		addMessage(FacesMessage.SEVERITY_INFO, "Informação", detalhes, identificador);
	}

	/**
	 * 
	 * @param detalhes
	 */
	public static void addWarning(String detalhes) {
		addMessage(FacesMessage.SEVERITY_WARN, "Alerta", detalhes);
	}

	/**
	 * 
	 * @param detalhes
	 * @param identificador
	 */
	public static void addWarning(String detalhes, String identificador) {
		addMessage(FacesMessage.SEVERITY_WARN, "Alerta", detalhes, identificador);
	}

	/**
	 * 
	 * @param detalhes
	 */
	public static void addError(String detalhes) {
		addMessage(FacesMessage.SEVERITY_ERROR, "Erro", detalhes);
	}

	/**
	 * 
	 * @param detalhes
	 * @param identificador
	 */
	public static void addError(String detalhes, String identificador) {
		addMessage(FacesMessage.SEVERITY_ERROR, "Erro", detalhes, identificador);
	}

	/**
	 * 
	 * @param detalhes
	 */
	public static void addFatal(String detalhes) {
		addMessage(FacesMessage.SEVERITY_FATAL, "Erro Grave", detalhes);
	}

	/**
	 * 
	 * @param detalhes
	 * @param identificador
	 */
	public static void addFatal(String detalhes, String identificador) {
		addMessage(FacesMessage.SEVERITY_FATAL, "Erro Grave", detalhes, identificador);
	}

	/**
	 * 
	 * @param severidade
	 * @param sumario
	 * @param detalhes
	 */
	public static void addMessage(FacesMessage.Severity severidade, String sumario, String detalhes) {
		addMessage(severidade, sumario, detalhes, null);
	}

	/**
	 * 
	 * @param severidade
	 * @param sumario
	 * @param detalhes
	 * @param identificador
	 */
	public static void addMessage(FacesMessage.Severity severidade, String sumario, String detalhes, String identificador) {
		if (FacesContext.getCurrentInstance() != null) {
			FacesContext.getCurrentInstance().addMessage(
				identificador,
				new FacesMessage(
					severidade,
					sumario,
					detalhes
				)
			);
		}
	}
}
