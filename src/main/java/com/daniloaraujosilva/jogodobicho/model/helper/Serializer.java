package com.daniloaraujosilva.jogodobicho.model.helper;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * Serializa e deserializa objetos utilizando o Jackson.
 * Os objetos são transformados ou destransformados em json.
 */
public class Serializer {

	/**
	 * 
	 */
	private static final ObjectMapper objectMapper = new ObjectMapper();

	/**
	 * 
	 */
	private Serializer() {
		objectMapper.setVisibility(PropertyAccessor.ALL, Visibility.NONE);
		objectMapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
		objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
	}

	/**
	 * 
	 */
	public static class Singleton {
		public static final Serializer INSTANCE = new Serializer();
	}

	/**
	 * 
	 * @return
	 */
	public static Serializer getInstance() {
		return Singleton.INSTANCE;
	}

	/**
	 * 
	 * @param input
	 * @return
	 * @throws JsonProcessingException
	 */
	public String serialize(Object input) throws JsonProcessingException {
		return objectMapper.writeValueAsString(input);
	}

	/**
	 * 
	 * @param input
	 * @param classeDeRetorno
	 * @return
	 * @throws JsonParseException
	 * @throws com.fasterxml.jackson.databind.JsonMappingException
	 * @throws IOException
	 */
	public <R> R unserialize(String input, Class<R> classeDeRetorno) throws JsonParseException, com.fasterxml.jackson.databind.JsonMappingException, IOException {
		return objectMapper.readValue(input, classeDeRetorno);
	}
}