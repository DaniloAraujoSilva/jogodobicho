package test;

import com.daniloaraujosilva.jogodobicho.model.configuration.General;
import com.daniloaraujosilva.jogodobicho.model.enums.configuration.ExecutionModeEnum;

/**
 *
 */
abstract public class AbstractTest {

	public AbstractTest() {
		super();
		General.setExecutionMode(ExecutionModeEnum.TEST);
	}
}