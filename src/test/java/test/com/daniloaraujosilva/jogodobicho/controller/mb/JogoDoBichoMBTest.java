package test.com.daniloaraujosilva.jogodobicho.controller.mb;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.daniloaraujosilva.jogodobicho.controller.mb.JogoDoBichoMB;
import com.daniloaraujosilva.jogodobicho.model.pojo.NodePojo;

import test.AbstractTest;

/**
 *
 */
public class JogoDoBichoMBTest extends AbstractTest {

	/**
	 *
	 */
	public JogoDoBichoMBTest() {
		super();
	}

	/**
	 *
	 */
	@Test
	public void encontrarTubarao() {
		JogoDoBichoMB mb = new JogoDoBichoMB();
		mb.init();
		mb.apagarInformacoes();
		mb.animalEscolhido();
		mb.animalPossuiAHabilidade(); // vive na água? sim
		mb.animalEscolhidoEncontrado(); // animal escolhido (Tubarão)? sim
		assertTrue(mb.getCurrent().getQuestion().equals("Tubarão"));
		mb.salvarERecomecar();
	}

	/**
	 *
	 */
	@Test
	public void encontrarMacaco() {
		JogoDoBichoMB mb = new JogoDoBichoMB();
		mb.init();
		mb.apagarInformacoes();
		mb.animalEscolhido();
		mb.animalNaoPossuiAHabilidade(); // vive na água? não
		mb.animalEscolhidoEncontrado(); // animal escolhido (Macaco)? sim
		assertTrue(mb.getCurrent().getQuestion().equals("Macaco"));
		mb.salvarERecomecar();
	}

	/**
	 *
	 */
	@Test
	public void cadastrarEEncontrarBaleia() {
		JogoDoBichoMB mb = new JogoDoBichoMB();
		mb.init();
		mb.apagarInformacoes();
		mb.animalEscolhido();
		mb.animalPossuiAHabilidade(); // vive na água? sim
		mb.animalEscolhidoNaoEncontrado(); // animal escolhido (Tubarão)? não
		mb.getCurrent().getYes().setQuestion("Baleia"); // qual animal? Baleia
		mb.getCurrent().setQuestion("é um mamífero"); // qual característica? é um mamífero
		mb.salvarERecomecar();
		mb.animalEscolhido();
		mb.animalPossuiAHabilidade(); // vive na água? sim
		mb.animalPossuiAHabilidade(); // é um mamífero? sim
		mb.animalEscolhidoEncontrado(); // animal escolhido (Baleia)? sim
		assertTrue(mb.getCurrent().getQuestion().equals("Baleia"));
		mb.salvarERecomecar();
	}

	/**
	 *
	 */
	@Test
	public void cadastrarBaleiaEGolfilhoEEncontrarGolfinho() {
		JogoDoBichoMB mb = new JogoDoBichoMB();
		mb.init();
		mb.apagarInformacoes();
		mb.animalEscolhido();
		mb.animalPossuiAHabilidade(); // vive na água? sim
		mb.animalEscolhidoNaoEncontrado(); // animal escolhido (Tubarão)? não
		mb.getCurrent().getYes().setQuestion("Baleia"); // qual animal? Baleia
		mb.getCurrent().setQuestion("é um mamífero"); // qual característica? é um mamífero
		mb.salvarERecomecar();
		mb.animalEscolhido();
		mb.animalPossuiAHabilidade(); // vive na água? sim
		mb.animalPossuiAHabilidade(); // é um mamífero? sim
		mb.animalEscolhidoNaoEncontrado(); // animal escolhido (Baleia)? não
		mb.getCurrent().getYes().setQuestion("Golfinho"); // qual animal? Golfinho
		mb.getCurrent().setQuestion("gosta de gente"); // qual característica? gosta de gente
		mb.salvarERecomecar();
		mb.animalEscolhido();
		mb.animalPossuiAHabilidade(); // vive na água? sim
		mb.animalPossuiAHabilidade(); // é um mamífero? sim
		mb.animalPossuiAHabilidade(); // gosta de gente? sim
		mb.animalEscolhidoEncontrado(); // animal escolhido (Golfinho)? sim
		assertTrue(mb.getCurrent().getQuestion().equals("Golfinho"));
		mb.salvarERecomecar();
	}

	/**
	 *
	 */
	@Test
	public void cadastrarBaleiaGolfinhoELeaoEEncontrarLeao() {
		JogoDoBichoMB mb = new JogoDoBichoMB();
		mb.init();
		mb.apagarInformacoes();
		mb.animalEscolhido();
		mb.animalPossuiAHabilidade(); // vive na água? sim
		mb.animalEscolhidoNaoEncontrado(); // animal escolhido (Tubarão)? não
		mb.getCurrent().getYes().setQuestion("Baleia"); // qual animal? Baleia
		mb.getCurrent().setQuestion("é um mamífero"); // qual característica? é um mamífero
		mb.salvarERecomecar();
		mb.animalEscolhido();
		mb.animalPossuiAHabilidade(); // vive na água? sim
		mb.animalPossuiAHabilidade(); // é um mamífero? sim
		mb.animalEscolhidoNaoEncontrado(); // animal escolhido (Baleia)? não
		mb.getCurrent().getYes().setQuestion("Golfinho"); // qual animal? Golfinho
		mb.getCurrent().setQuestion("gosta de gente"); // qual característica? gosta de gente
		mb.salvarERecomecar();
		mb.animalEscolhido();
		mb.animalNaoPossuiAHabilidade(); // vive na água? não
		mb.animalEscolhidoNaoEncontrado(); // animal escolhido (Macaco)? não.
		mb.getCurrent().getYes().setQuestion("Leão"); // qual animal? Leão
		mb.getCurrent().setQuestion("tem juba"); // qual característica? tem juba
		mb.salvarERecomecar();
		mb.animalEscolhido();
		mb.animalNaoPossuiAHabilidade(); // vive na água? não
		mb.animalPossuiAHabilidade(); // tem juba? sim
		mb.animalEscolhidoEncontrado(); // animal escolhido (Leão)? sim
		assertTrue(mb.getCurrent().getQuestion().equals("Leão"));
		mb.salvarERecomecar();
	}

	/**
	 *
	 */
	@Test
	public void cadastrarBaleiaGolfinhoLeaoEEncontrarUmPorUm() {
		JogoDoBichoMB mb = new JogoDoBichoMB();
		mb.init();
		mb.apagarInformacoes();
		mb.animalEscolhido();
		mb.animalPossuiAHabilidade(); // vive na água? sim
		mb.animalEscolhidoNaoEncontrado(); // animal escolhido (Tubarão)? não
		mb.getCurrent().getYes().setQuestion("Baleia"); // qual animal? Baleia
		mb.getCurrent().setQuestion("é um mamífero"); // qual característica? é um mamífero
		mb.salvarERecomecar();
		mb.animalEscolhido();
		mb.animalPossuiAHabilidade(); // vive na água? sim
		mb.animalPossuiAHabilidade(); // é um mamífero? sim
		mb.animalEscolhidoNaoEncontrado(); // animal escolhido (Baleia)? não
		mb.getCurrent().getYes().setQuestion("Golfinho"); // qual animal? Golfinho
		mb.getCurrent().setQuestion("gosta de gente"); // qual característica? gosta de gente
		mb.salvarERecomecar();
		mb.animalEscolhido();
		mb.animalNaoPossuiAHabilidade(); // vive na água? não
		mb.animalEscolhidoNaoEncontrado(); // animal escolhido (Macaco)? não.
		mb.getCurrent().getYes().setQuestion("Leão"); // qual animal? Leão
		mb.getCurrent().setQuestion("tem juba"); // qual característica? tem juba
		mb.salvarERecomecar();
		mb.animalEscolhido();
		mb.animalPossuiAHabilidade(); // vive na água? sim
		mb.animalNaoPossuiAHabilidade(); // é um mamífero? não
		mb.animalEscolhidoEncontrado(); // animal escolhido (Tubarão)? sim
		assertTrue(mb.getCurrent().getQuestion().equals("Tubarão"));
		mb.salvarERecomecar();
		mb.animalEscolhido();
		mb.animalPossuiAHabilidade(); // vive na água? sim
		mb.animalPossuiAHabilidade(); // é um mamífero? sim
		mb.animalNaoPossuiAHabilidade(); // gosta de gente? não
		mb.animalEscolhidoEncontrado(); // animal escolhido (Baleia)? sim
		assertTrue(mb.getCurrent().getQuestion().equals("Baleia"));
		mb.salvarERecomecar();
		mb.animalEscolhido();
		mb.animalPossuiAHabilidade(); // vive na água? sim
		mb.animalPossuiAHabilidade(); // é um mamífero? sim
		mb.animalPossuiAHabilidade(); // gosta de gente? sim
		mb.animalEscolhidoEncontrado(); // animal escolhido (Golfinho)? sim
		assertTrue(mb.getCurrent().getQuestion().equals("Golfinho"));
		mb.salvarERecomecar();
		mb.animalEscolhido();
		mb.animalNaoPossuiAHabilidade(); // vive na água? não
		mb.animalNaoPossuiAHabilidade(); // tem juba? não
		mb.animalEscolhidoEncontrado(); // animal escolhido (Macaco)? sim
		assertTrue(mb.getCurrent().getQuestion().equals("Macaco"));
		mb.salvarERecomecar();
		mb.animalEscolhido();
		mb.animalNaoPossuiAHabilidade(); // vive na água? não
		mb.animalPossuiAHabilidade(); // tem juba? sim
		mb.animalEscolhidoEncontrado(); // animal escolhido (Leão)? sim
		assertTrue(mb.getCurrent().getQuestion().equals("Leão"));
	}

	@Test
	public void verificarArvore() {
		JogoDoBichoMB mb = new JogoDoBichoMB();
		mb.init();
		mb.apagarInformacoes();
		mb.animalEscolhido();
		mb.animalPossuiAHabilidade(); // vive na água? sim
		mb.animalEscolhidoNaoEncontrado(); // animal escolhido (Tubarão)? não
		mb.getCurrent().getYes().setQuestion("Baleia"); // qual animal? Baleia
		mb.getCurrent().setQuestion("é um mamífero"); // qual característica? é um mamífero
		mb.salvarERecomecar();
		mb.animalEscolhido();
		mb.animalPossuiAHabilidade(); // vive na água? sim
		mb.animalPossuiAHabilidade(); // é um mamífero? sim
		mb.animalEscolhidoNaoEncontrado(); // animal escolhido (Baleia)? não
		mb.getCurrent().getYes().setQuestion("Golfinho"); // qual animal? Golfinho
		mb.getCurrent().setQuestion("gosta de gente"); // qual característica? gosta de gente
		mb.salvarERecomecar();
		mb.animalEscolhido();
		mb.animalNaoPossuiAHabilidade(); // vive na água? não
		mb.animalEscolhidoNaoEncontrado(); // animal escolhido (Macaco)? não.
		mb.getCurrent().getYes().setQuestion("Leão"); // qual animal? Leão
		mb.getCurrent().setQuestion("tem juba"); // qual característica? tem juba
		mb.salvarERecomecar();

		NodePojo root = mb.getCurrent();
		assertTrue(root.getQuestion().equals("vive na água"));
			assertTrue(root.getYes().getQuestion().equals("é um mamífero"));
				assertTrue(root.getYes().getYes().getQuestion().equals("gosta de gente"));
					assertTrue(root.getYes().getYes().getYes().getQuestion().equals("Golfinho"));
					assertTrue(root.getYes().getYes().getNo().getQuestion().equals("Baleia"));
				assertTrue(root.getYes().getNo().getQuestion().equals("Tubarão"));
			assertTrue(root.getNo().getQuestion().equals("tem juba"));
				assertTrue(root.getNo().getYes().getQuestion().equals("Leão"));
				assertTrue(root.getNo().getNo().getQuestion().equals("Macaco"));
	}
}